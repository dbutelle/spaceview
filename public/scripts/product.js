document.addEventListener('DOMContentLoaded',function(){
    //Déclaration des variables de récuparation
    let butmoins=document.getElementById('button_moins')
    let butplus=document.getElementById('button_plus')
    let chiffre=document.getElementById('chiffre')
    let div=document.createElement('div')
    let form=document.getElementById('form')
    let minia=document.querySelector(".product-miniatures")
    let grandeImg= document.getElementById('grandeImg')
    let imgMinia=minia.getElementsByTagName('img')
    let get_number=document.getElementById('get_number')

    console.log(get_number)

    //Si il y a un clique sur le bouton '+'
    butplus.addEventListener('click',function (){
        //Si le chiffre est à 4 (donc qu'il y a 5 cliques sur le +)
        if(chiffre.textContent==='4'){
            //On donne les classes qui vont bien
            div.classList.add('box')
            div.classList.add('error')
            //On lui met un petit id
            div.setAttribute('id','divError')
            //on lui ajoute le texte
            div.innerHTML="Quantité maximale autorisée !"
            //On l'ajoute au form
            form.appendChild(div)
        }
        //Si c'est différent de 5
        if(chiffre.textContent!=='5'){
            //on incrémente le bouton du milieu de 1
            chiffre.innerHTML++

        }
        get_number.setAttribute('value',chiffre.innerHTML)
    })
    //Si il y a un clique sur le bouton '-'
    butmoins.addEventListener('click',function (){
        //Si le chiffre du milieu est différent de 1
        if(chiffre.textContent!=1){
            //On le décrémente de 1
            chiffre.innerHTML--
        }
        //Déclaration de la variable de récupération de mon div d'erreur
        let divError=document.getElementById('divError')
        //Si le div d'erreur existe
        if(document.body.contains(divError)){
            //On le retire
            form.removeChild(div)
        }
        get_number.setAttribute('value',chiffre.innerHTML)
    })
    //Pour toutes les images miniatures
        for(let imgs of imgMinia){
            //Si on clique sur l'une d'entre elle
            imgs.addEventListener('click',function(){
                //On change la source de la grosse image par celle de la petite sur laquelle on a cliqué
                grandeImg.setAttribute('src',imgs.src)
            })
        }

})

