document.addEventListener('DOMContentLoaded',function(){

        let nom = document.getElementsByName('userlastname')
        let prenom = document.getElementsByName('userfirstname')
        let mail = document.getElementsByName('usermail')
        let password=document.getElementsByName('userpass')
        let form = document.getElementById('signin')
        let p=form.getElementsByTagName('p')
        var emailReg = new RegExp(/^([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/i);
        var passReg= new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/);

        nom[0].addEventListener('change',function (){
            if(nom[0].value.length<=2){
                nom[0].classList.remove('valid')
                nom[0].classList.add('invalid')
                p[0].classList.remove('valid')
                p[0].classList.add('invalid')

            }else{
                nom[0].classList.remove('invalid')
                nom[0].classList.add('valid')
                p[0].classList.remove('invalid')
                p[0].classList.add('valid')

            }
        })




        prenom[0].addEventListener('change',function (){
            if(prenom[0].value.length<=2){
                prenom[0].classList.remove('valid')
                prenom[0].classList.add('invalid')
                p[1].classList.remove('valid')
                p[1].classList.add('invalid')

            }else{
                prenom[0].classList.remove('invalid')
                prenom[0].classList.add('valid')
                p[1].classList.remove('invalid')
                p[1].classList.add('valid')

            }
        })



        mail[1].addEventListener('change',function(){
            if(!emailReg.test(mail[1].value)){
                mail[1].classList.remove('valid')
                mail[1].classList.add('invalid')
                p[2].classList.remove('valid')
                p[2].classList.add('invalid')

            }else{
                mail[1].classList.remove('invalid')
                mail[1].classList.add('valid')
                p[2].classList.remove('invalid')
                p[2].classList.add('valid')

            }

        })


        password[1].addEventListener('change',function(){
            if(!passReg.test(password[1].value)){
                password[1].classList.remove('valid')
                password[1].classList.add('invalid')
                p[3].classList.remove('valid')
                p[3].classList.add('invalid')

            }else{
                password[1].classList.remove('invalid')
                password[1].classList.add('valid')
                p[3].classList.remove('invalid')
                p[3].classList.add('valid')

            }

        })
        password[2].addEventListener('change',function(){
            if(password[1].value!==password[2].value){
                password[2].classList.remove('valid')
                password[2].classList.add('invalid')
                p[4].classList.remove('valid')
                p[4].classList.add('invalid')

            }else{
                password[2].classList.remove('invalid')
                password[2].classList.add('valid')
                p[4].classList.remove('invalid')
                p[4].classList.add('valid')

            }


        })
})
        function valider(){
            let prenom = document.getElementsByName('userfirstname')
            let mail = document.getElementsByName('usermail')
            let password=document.getElementsByName('userpass')
            var emailReg = new RegExp(/^([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/i);
            var passReg= new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/);
            let nom = document.getElementsByName('userlastname')
            if(nom[0].value.length<=2 || prenom[0].value.length<=2 || !emailReg.test(mail[1].value) ||
                !passReg.test(password[1].value) || password[1].value!==password[2].value) {
                return false
            }
            else{
                return true;
            }
        }




