<?php
if (isset($params['valid']) && $params['valid'] == "post_success") {
    ?>
    <div class="box" id="valid">
        <h3>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check2-circle" viewBox="0 0 16 16">
                <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0z"/>
                <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l7-7z"/>
            </svg>
            Votre avis a été publié
        </h3>
    </div>
    <br> <br>
    <?php
} elseif (isset($params['valid']) && $params['valid'] == "post_failed") {
    ?>
    <div class="box" id="invalid">
        <h3>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
            </svg>
            Votre avis n'a pas été publié
        </h3>
    </div>
    <br> <br>
    <?php
}
?>

<div style="background-color: white" id="product">
    <?php foreach ($params['infos'] as $info) { ?>
        <div>
            <div class="product-images">
                <img id='grandeImg' src="/public/images/<?php echo $info['image'] ?>"/>

                <div class='product-miniatures'>
                    <div>
                        <img src="/public/images/<?php echo $info['image'] ?>"/>
                    </div>

                    <?php for ($i = 1; $i <= 3; $i++) { ?>
                        <div>
                            <img src="/public/images/<?php echo $info['image_alt' . $i]; ?> "/>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="product-infos">
                <p class="product-category">
                    <?php echo $info["category"]; ?>
                </p>
                <h1 style="color:#122640">
                    <?php echo $info['name']; ?>
                </h1>
                <p style="color:#122640" class="product-price">
                    <?php echo $info['price']; ?>€
                </p>
                <form id="form" method="post" action="/store/cart/add">
                    <button type="button" id="button_moins" style="color:#122640;border-color: #122640">-</button>
                    <input type="hidden" name="name" value="<?= $info['id'] ?>">
                    <input type="hidden" id="get_number" name="chiffre" value="1"/>
                    <button type="button"><span id="chiffre" style="color:#122640;border-color: #122640">1</span></button>
                    <button type="button" id="button_plus" style="color:#122640;border-color: #122640">+</button>
                    <input type="submit" style="color:#122640;border-color: #122640" value="Ajouter au panier"/>
                </form>
            </div>
        </div>
        <div>
            <div class="product-spec">
                <h2>Spécificités</h2>
                <?php echo $info['spec']; ?>
            </div>
            <div class="product-comments">
                <h2>
                    <?php if (count($params['comments']) > 0) {
                        echo count($params['comments']);
                    } ?>
                    Avis

                </h2>

                <?php
                foreach ($params["comments"] as $comments) {
                    ?>
                    <div class="product-comment">
                        <p class="product-comment-author">
                            <?php
                            echo $comments['firstname'];
                            echo " ";
                            echo $comments['lastname'];

                            ?>
                            <span> a écrit :</span>
                        </br>

                        </p>
                        <p>"
                            <?php echo $comments['content']; ?>
                            "
                        </br>
                            <span style="font-size: 12px;">Avis publié le <?php echo $comments['date'];?></span>
                        </p>

                    </div>
                    <?php
                }
                ?>

                <?php
                if (count($params['comments']) <= 0) {
                    ?>
                    <div class="product-comment">
                        <p class="product-comment-author">Il n'y a pas encore d'avis</p>
                        <h5 style="margin-bottom: -5px">
                            Soyez le premier à donner votre avis !
                        </h5>
                    </div>
                    <?php
                }
                ?>

                <?php if (isset($_SESSION['id'])) { ?>
                    <form method="post" action="/store/postComment">
                        <input type="hidden" name="product-id" value="<?php echo $info["id"]; ?>">
                        <input type="text" id="newComment" name="newComment" placeholder="Rédigez un commentaire">
                    </form>
                    <?php
                } else {
                    ?>
                    <p style="margin-top: 30px">Vous n'êtes pas connecté.
                        <a href="/account" style="text-decoration: underline">Connectez-vous</a>
                        pour rédiger un commentaire
                    </p>
                <?php } ?>

            </div>
        </div>
    <?php } ?>
</div>
<script type="text/javascript" src="/public/scripts/product.js"></script>