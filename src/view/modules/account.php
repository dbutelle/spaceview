<script type="text/javascript" src="/public/scripts/signin.js"></script>
<?php
if((!empty($_GET['status']))){


    if($_GET['status']=='login_fail'){
        ?>
        <div class="box error" style="margin:40px;">
            <p>La connexion a échoué. Vérifiez vos identifiants et réessayer</p>
        </div>
        <?php
    }
    if($_GET['status']=='signin_success'){
        ?>
        <div class="box info" style="margin:40px;">
            <p>Inscription réussie ! Vous pouvez dés à présent vous connecter.</p>
        </div>
        <?php
    }
    if($_GET['status']=='logout_success'){
        ?>
        <div class="box info" style="margin:40px;">
            <p>Vous avez été déconnecté avec succés.</p>
        </div>
        <?php
    }
}
?>
<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" style="color:white;" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin" onsubmit="return valider();" id="signin">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p>Nom</p>
  <input type="text" name="userlastname" placeholder="Nom" />

  <p>Prénom</p>
  <input type="text" name="userfirstname" placeholder="Prénom" />

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <p>Répéter le mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" style="color: white" value="Inscription" />

</form>

</div>
