

<div id="cart">

        <h1 style="margin: 50px">Mon panier</h1>
        <?php if(isset($_SESSION['cart'])){
            //print_r($_SESSION['cart']);
        $cart = $_SESSION['cart'];
            ?>

        <div >
            <?php
            $total = 0;
            foreach ($cart as $product) {

                $infos = \model\StoreModel::infoProduct($product['id'])[0];
                $total += $infos['price'] * $product['chiffre']; ?>

                <div class="cart">

                        <img src="/public/images/<?php echo $infos['image']; ?>" alt="<?php echo $infos['name']; ?>">


                    <div class="product-name-cart">
                        <p class="product-category"><?php echo $infos["category"]; ?></p>
                        <h3><?php echo $infos['name']; ?></h3>
                    </div>

                    <form method="post" action="/cart/add">
                    <div class="cart-quantity">
                        <h5 style="font-size: 20px;color:var(--bg-main);">Quantité</h5>
                        <div style="display: flex;">
                            <button type="button" id="button_moins">-</button>
                            <input type="hidden" name="name" value="<?php $infos['id']; ?>">

                            <button type="button" name="chiffre"><span id="chiffre"><?php echo $product['chiffre']?></span></button>
                            <button type="button" id="button_plus">+</button>
                        </div>
                    </div>
                    </form>

                    <div class="cart-product-quantity">
                        <h5>Prix unitaire</h5>
                        <h2><?php echo $infos['price']; ?>€</h2>
                    </div>
                </div>

            <?php } ?>
                <div class="cartTotal">
                    <h2>Prix total du panier :</h2>
                    <h3 style="font-size: 40px"><?php echo $total; ?>€</h3>
                </div>
            <?php }else { ?>
                <div class="box warning">
                    Le panier est vide.
                </div>
            <?php } ?>
        </div>
</div>
