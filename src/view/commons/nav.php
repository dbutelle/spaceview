<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="/public/styles/default.css" />

<nav>
    <img src="/public/images/avatar_login.svg"/>
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>
    <?php
        if(!empty($_SESSION)){
            ?>
            <a class="account" href="/account/infos"><?php echo $_SESSION['prenom'].' '.$_SESSION['nom'];?></a>
            <a href="/store/cart">Panier</a>
            <a href="/account/logout">Déconnexion</a>
    <?php
        }else{
            ?>
            <a class="account" href="/account">Compte</a>
    <?php
        }
    ?>



</nav>
