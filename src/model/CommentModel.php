<?php


namespace model;


class CommentModel
{
    public static function insertComment(int $id,int $productId, string $msg):bool{
        $db = Model::connect();

        $sql = "INSERT INTO comment(comment.id,comment.content, comment.date, comment.id_product, comment.id_account) VALUES('','$msg',NOW(),'$productId','$id')";

        $req = $db->prepare($sql);
        $req->execute();

        return true;

    }
    public static function listComment(int $productId): array
    {
        $db = Model::connect();

        $sql = "SELECT comment.content, account.lastname AS lastname, account.firstname AS firstname, comment.date FROM comment 
        INNER JOIN account ON comment.id_account = account.id WHERE comment.id_product = '$productId' ORDER BY comment.date";

        $req = $db->prepare($sql);
        $req->execute();

        return $req->fetchAll();

    }
}