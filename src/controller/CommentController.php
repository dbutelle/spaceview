<?php


namespace controller;

use model\CommentModel;
class CommentController
{
    public static function postComment():void{
        $comment = CommentModel::insertComment($_SESSION['id'], $_POST['product-id'],$_POST['newComment']);

        if ($comment==true){
            StoreController::product($_POST['product-id'], "post_success");
        }
        else{
            StoreController::product($_POST['product-id'], "post_failed");
        }


    }
}