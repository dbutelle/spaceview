<?php

namespace controller;

class ErrorController {

  public function error(): void
  {
      $params = array(
          "title" => "Error",
          "module" => "error.php",
      );
      \view\Template::render($params);
  }

}