<?php


namespace controller;


class AccountController
{
    public function account(): void
    {
        $params = [
            "title" => "Account",
            "module" => "account.php"
        ];
        \view\Template::render($params);
    }

    public function login(): void
    {

        $mail = trim(htmlentities($_POST['usermail']));
        $password = trim(htmlentities($_POST['userpass']));
        $verif = \model\AccountModel::login($mail, $password);

        if (count($verif) == 1) {
            foreach ($verif as $compte) {
                $_SESSION['id'] = $compte['id'];
                $_SESSION['prenom'] = $compte['firstname'];
                $_SESSION['nom'] = $compte['lastname'];
                $_SESSION['mail'] = $compte['mail'];
                header('Location:/store');
            }
        } else {

           header('Location:/account?status=login_fail');
        }

    }

    public function signin(): void
    {
        $firstname = trim(htmlentities($_POST['userfirstname']));
        $lastname = trim(htmlentities($_POST['userlastname']));
        $mail = trim(htmlentities($_POST['usermail']));
        $password = trim(htmlentities($_POST['userpass']));

        \model\AccountModel::signin($firstname, $lastname, $mail, $password);
        header('Location:/account?status=signin_success');
    }
    public function logout():void{
        session_destroy();
        header('Location:/account?status=logout_success');
    }
    public function infos():void{

        $params = [
            "title" => "Personnal account",
            "module" => "infos.php"
        ];
        \view\Template::render($params);
    }
    public function update():void{
        $update = \model\AccountModel::update(htmlspecialchars($_POST['infosprenom']), htmlspecialchars($_POST['infosnom']), htmlspecialchars($_POST['infosmail']));
        if($update==true){
            $_SESSION['prenom']=htmlspecialchars($_POST['infosprenom']);
            $_SESSION['nom']=htmlspecialchars($_POST['infosnom']);
            $_SESSION['mail']=htmlspecialchars($_POST['infosmail']);
            $_GET['status']="changed";
        }else{
            $_GET['status']="not_changed";
        }

        $params = [
            "title" => "Personnal account",
            "module" => "infos.php",
            "valid"=>($_GET['status']??null)
        ];
        \view\Template::render($params);
    }
}